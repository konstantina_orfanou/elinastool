﻿'use strict';

// common app properties ------------------------------------------------------------------------------
var app_Name = "xClient",
    app_Version = "1.0.0.1";
//-----------------------------------------------------------

(function () {
    var commons;

    /**
     * pushes the object in the array if there is no other object with object.prop === val in the array
     * @param  {Object} what    the object to be pushed into the array
     * @param  {string} prop    the property of the object to search for uniqueness
     * @param  {mixed} val      the value of the property
     * @return {Boolean}        returns true on push to array else false
     */
    Array.prototype.pushIfUniquePropVal = function (what, prop, val) {
        var found;
        this.forEach(function (item, index) {
            if (item[prop] === val) found = true; return;
        });
        if (found) return false;
        this.push(what);
        return true;
        // returns true if object was pushed in array
    }

    /**
     * pushes value to the array if it is not already there
     * @param {mixed} value the value to be pushed into the array
     * @return {bool} returns true of the item was pushed into the array
     */
    Array.prototype.addUnique = function (value) {
        //
        if (this.indexOf(value) !== -1) {
            return false;
        }
        this.push(value);
        return true;
    }

    /* extend jQuery to textify a link (<a> tag) */
    $.fn.textify = function () {
        this.each(function () {
            $(this).replaceWith($(this).text());
        });
    }

    /* extend jQuery dataTables to sort date 
     * take attention of dates like 4/1/2015 -> 04/01/2015
     * in case already 04/01/2015 do not add second 0
     */
    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
            if (a == null || a == "") {
                return 0;
            }
            var ukDatea = a.split('/');
            return (
                ukDatea[2]
                + (ukDatea[1] * 1 < 10 ? '0' + (ukDatea[1] * 1) : ukDatea[1])
                + (ukDatea[0] * 1 < 10 ? '0' + (ukDatea[0] * 1) : ukDatea[0])
                ) * 1;
        },

        "date-uk-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "date-uk-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });

    /**
     * Capitalizes the first letter of the string
     * @return {string}
     */
    String.prototype.ucfirst = function () {
        return this.charAt(0).toUpperCase() + this.substr(1);
    }

    /**
     * splits the string with the space " " and capitalizes
     * the first letter of every word
     * @return {[type]} [description]
     */
    String.prototype.camelCase = function () {
        return this.split(" ").map(function (strElement) {
            return strElement.ucfirst();
        }).join(" ");
    }
    //#endregion ----------------------------------------------------------- extensions

    //#region ----------------------------------------------------- common functions

    /*
     * @description transforms empty strings to '-'
     * @param  {String} str  the entry string
     * @return {String}     returns '-' in case str is empty otherwise, the str unchanged
     */
    function notEmtyString(str) {
        return (str === null || str === undefined || str === "") ? "-" : str;
    }

    /**
     * @description given an template and an key-value object,it replaces the strings wrapped in {{}} with values from the obj
     * @param   {String} template the template to render
     * @param   {Object} obj      the object to use its properties to replace strings in template
     * @returns {String}          the template rendered
     */
    function renderTemplate(template, obj) {

        var keysToFind = [],
            rendered = template;

        var regex = new RegExp("{{(.*?)}}", "g");
        keysToFind = template.match(regex);

        keysToFind.forEach(function (key) {
            var k = key.replace("{{", "").replace("}}", "");
            var val = obj[k];

            if (val != null) {
                // != instead of !== because we want to avoid entering this block if undefined
                // boolean values to String
                val = (val.toString().toLowerCase() === "false") ? "false" : val;
                val = (val.toString().toLowerCase() === "true") ? "true" : val;
            } else {
                val = "";
            }

            rendered = rendered.replace(key, val);
        });

        return rendered;
    }

    /*
     * @description find the max value of a given attribute
     * @param   {Array} array         an array of objects 
     * @param   {String} prop         objects' property with int value
     * @returns {Int}  maxNumber      the max value of prop
     */
    function findMaxNumber(array, prop) {
        var maxNumber = 0;
        if (array && array.length !== 0) {
            maxNumber = Math.max.apply(Math, array.map(function (obj) {
                return obj[prop];
            }));
            if (isNaN(maxNumber)) {
                maxNumber = 0;
            }
        }
        return maxNumber;
    }

    /*
     * @description       checks if a given variable is null or undefined or empty string
     * @param   {Var} variable  the given variable to be checked
     * @returns {Boolean}       depending on the result of the condition
     */
    function isEmpty(variable) {
        if (variable === null || typeof (variable) === 'undefined') {
            return true;
        }
        else if (typeof (variable) === 'string') {
            return variable === "" ? true : false;
        }
        else {
            return false;
        }
    }

    /*
     * @description                     gets the index of given tab selector
     * @param   {String} tabSelector    the given tab selector to be checked
     * @returns {Int}                   the tab's index in tabs
     */
    function getTabIndex(tabSelector) {
        var tmpStr = tabSelector.split('-');
        return parseInt(tmpStr[1]) - 1;
    }

    commons = {
        renderTemplate: renderTemplate,
    };

    console.log("app_commons.js is loaded");

    // expose commons
    window.commons = commons;
})();