'use strict';

(function () {
    var xmapService;

    var localTileLayer;
    var myMap;

    function RenderMap() {
        this.getMap = function (x, y) {
            myMap = new L.Map('mymap', { center: [x, y], zoom: 13 });
            return myMap;

        };


        this.getTileLayer = function () {
            var urlTemplate = 'http://xserver-2:40000/services/rest/XMap/tile/{z}/{x}/{y}/gravelpit+PTV_TruckAttributes';
            return new L.TileLayer(urlTemplate, {
                minZoom: 1,
                maxZoom: 19,
                attribution: 'PTV, TomTom'
            }
            );
        };
    }

    function UpdateMap(tileLayer) {

        this.updateMap = function (response, tileLayer) {
            localTileLayer = tileLayer;
        };

        this.getTileLayer = function (localTileLayer) {
            return localTileLayer;
        }


    }

    xmapService = {
        RenderMap: RenderMap,
        UpdateMap: UpdateMap,
    };

    console.log("xmap.js is loaded");

    // expose xmapService
    window.xmapService = xmapService;
})();
  