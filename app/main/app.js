﻿'use strict';

$(document).ready(function () {

    // initialize tabs with ui jquery
    $('#tabs').tabs(
        {
            // active Tab stored in sessionStorage, otherwise the first
            active: sessionStorage.aat ? commons.getTabIndex(sessionStorage.aat) : firstTabIndex,
            beforeActivate: function (event, ui) {
                var activeTab = ui.newPanel.selector;
                checkTabAction(activeTab);
                // save currently active tab in session storage
                sessionStorage.setItem('aat', activeTab);
            },
            disabled: true,
            heightStyle: "content",
        });

});

// new tab selected, check what to do
function checkTabAction(selector) {
    var dataType = $(selector).data("type");
    //say(dataType);
    switch (dataType) {
        case "timetable":
            getTimetable(selector);
            break;
        case "activities":
            getActivities(selector);
            break;
        case "exams":
            getExamsTimetable(selector);
            break;
        case "grades":
            getGrades(selector);
            break;
        case "absences":
            getAbsences(selector);
            break;
        case "routes":
            getRoutesTimetable(selector);
            break;
        default:
    }
}

// Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon
function myFunction() {
    var x = document.getElementById("nav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}