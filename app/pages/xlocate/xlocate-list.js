'use strict';

(function () {
    var xlocateList;

	function XlocateListConstroller( Xlocate, RequestService, XlocateService, RenderMap, UpdateMap){
			var self = this; 	
			self.locations = [];
            
			self.orderProp = "city";

			var layer;
			if (self.map == null){
				self.map = xmapService.RenderMap.getMap(49.008083301,  8.4037561426);
				
				 
			}
			 
			self.tileLayer = xmapService.RenderMap.getTileLayer().addTo(self.map);

			//xLocate 2.0 request
			self.sendRequest = function(){
				self.displayCollection = [];
				var request =   { 
					  "searchObject": {
						"$type": "AddressLine",
						"text": self.inputcity
						  }
					  };

			xlocateService.sendRequest(request).then(function(response){
					
					
					var obj = response.data.results;
					for(var i=0; i<obj.length; i++){
						var location = obj[i].location;
						self.locations[i] = location;
						console.log(self.locations);
					}
					self.displayCollection = [].concat(self.locations);
										
			
					//Map stuff
					var pois = [];
					var coords = [];
					var x;
					var y;
					self.tileLayer = xmapService.RenderMap.getTileLayer().addTo(self.map);
					if(layer != null){
						self.map.removeLayer(layer);
					}
					
					for(var i=0; i<response.data.results.length; i++){
						if(i==0){
							x = response.data.results[i].location.referenceCoordinate.x;
							y = response.data.results[i].location.referenceCoordinate.y;
						}
						pois.push({
							"type": "Feature",
							"properties":{
								"name" : response.data.results[i].location.address.formattedAddress,
							},
							"geometry":{
								"type": "Point",
								"coordinates": [response.data.results[i].location.referenceCoordinate.x, response.data.results[i].location.referenceCoordinate.y]
							},

						});
						
						 coords.push([
							response.data.results[i].location.referenceCoordinate.y,
							response.data.results[i].location.referenceCoordinate.x
						]);						
					}

					xmapService.UpdateMap.updateMap(response.data.results, self.tileLayer);
						layer = new L.GeoJSON(pois).addTo(self.map);
						self.map.fitBounds(coords);
						
				});			
			}
			self.setSelected = function(location){
			self.map.setZoom(15);
			self.map.panTo([location.referenceCoordinate.y, location.referenceCoordinate.x]);
			
			};

	}
		
	xlocateList = {
	    sendRequest: se,
	    SetXlocateRequest: SetXlocateRequest,
	};

	say("xlocate-list.js is loaded");

    // expose xlocateList
	window.xlocateList = xlocateList;

})();
	