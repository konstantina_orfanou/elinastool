'use strict';
(function () {
    var checkmarkFilter;

    function checkmark(input) {
        return input ? '\u2713' : '\u2718';
    }

    checkmarkFilter = {
        checkmark: checkmark,
    };

    console.log("checkmark.filter.js is loaded");

    // expose checkmark
    window.checkmarkFilter = checkmarkFilter;

})();
