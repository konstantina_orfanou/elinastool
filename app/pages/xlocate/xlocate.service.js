﻿'use strict';

(function () {
    var xlocateService;

    function SetRequestParameter() {
        this.set = function (city) {
            console.log(city);
        }
        this.get = function () {
            return self.request;
        }
    }

    function SetXlocateRequest($http) {

        this.sendRequest = function (request, callback) {
            return $http({
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                data: request,
                url: "http://xserver-2:50000/services/rs/XLocate/2.1/searchLocations/"
            });
        };

    }

    xlocateService = {
        SetRequestParameter: SetRequestParameter,
        SetXlocateRequest: SetXlocateRequest,
    };

    say("xlocate.service.js is loaded");

    // expose xlocateService
    window.xlocate = xlocateService;
})();